package problema1;

public class Circle extends Forms {
	private double radius;

	public Circle(double radius) {
		this.radius = radius;
	}

	public double calculatePerimeter() {
		System.out.println("Perimeter circle:");
		return 2 * Pi * radius;
	}

	public double calculateArea() {
		System.out.println("Perimeter area:");
		return 2 * Pi * Math.pow(radius, 2);
	}
}
