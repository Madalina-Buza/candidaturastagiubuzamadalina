package problema1;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		String geometricFigure;
		Scanner sc = new Scanner(System.in);
		while (true) {
			System.out.println("What shape do you want? \n triangle \n circle \n rectangle \n exit \n shape = ");
			geometricFigure = sc.nextLine();
			switch (geometricFigure) {
			case "triangle": {
				String side1, side2, side3;
				System.out.print("side1 = ");
				side1 = sc.nextLine();
				System.out.print("side2 = ");
				side2 = sc.nextLine();
				System.out.print("side3 = ");
				side3 = sc.nextLine();
				Triangle triangle = new Triangle(Double.parseDouble(side1), Double.parseDouble(side2),
						Double.parseDouble(side3));
				if (triangle.isTriangle()) {
					System.out.println(triangle.calculateArea());
					System.out.println(triangle.calculatePerimeter());
				} else {
					System.out.println("Input values doesn't form an triangle!");
				}
				break;
			}
			case "circle": {
				String radius;
				System.out.print("radius = ");
				radius = sc.nextLine();
				Circle circle = new Circle(Double.parseDouble(radius));
				System.out.println(circle.calculateArea());
				System.out.println(circle.calculatePerimeter());
				break;
			}
			case "rectangle": {
				String length1, length2;
				System.out.print("length1 = ");
				length1 = sc.nextLine();
				System.out.print("length2 = ");
				length2 = sc.nextLine();
				Rectangle rectangle = new Rectangle(Double.parseDouble(length1), Double.parseDouble(length2));
				System.out.println(rectangle.calculateArea());
				System.out.println(rectangle.calculatePerimeter());
				break;
			}
			case "exit": {
				sc.close();
				System.exit(0);
			}
			default:
				System.out.println("Wrong choise!");
			}			
		}
	}
}
