package problema1;

public class Rectangle extends Forms {

	private double length1, length2;

	public Rectangle(double length1, double length2) {
		this.length1 = length1;
		this.length2 = length2;
	}

	public double calculatePerimeter() {
		System.out.println("Perimeter rectangle: ");
		return 2 * (length1 + length2);
	}

	public double calculateArea() {
		System.out.print("Area rectangle: ");
		return length1 * length2;
	}
}
