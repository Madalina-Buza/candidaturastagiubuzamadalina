package problema1;

public abstract class Forms {

	protected double Pi = 3.14;

	public abstract double calculatePerimeter();

	public abstract double calculateArea();

}
